mod camera;
mod components;
mod constants;
mod map;
mod map_builder;
mod spawner;
mod systems;
mod turn_state;

mod prelude {
    pub use anyhow::Result;
    pub use bracket_lib::prelude::*;
    pub use contracts::*;
    pub use crate::camera::*;
    pub use crate::components::*;
    pub use crate::constants::*;
    pub use crate::map::*;
    pub use crate::map_builder::*;
    pub use crate::spawner::*;
    pub use crate::systems::*;
    pub use crate::turn_state::*;
    pub use dotenv::dotenv;
    pub use lazy_static::*;
    pub use legion::*;
    pub use legion::world::SubWorld;
    pub use legion::systems::CommandBuffer;
    pub use log::{debug, error, info, warn};
}

use prelude::*;
use std::sync::{Once};

struct State {
    ecs : World,
    resources : Resources,
    input_systems : Schedule,
    player_systems : Schedule,
    monster_systems : Schedule,
}

impl State {
    fn new() -> Self {
        let mut ecs = World::default();
        let mut resources = Resources::default();
        let mut rng = RandomNumberGenerator::new();
        let map_builder = MapBuilder::new(&mut rng);
        spawn_player(&mut ecs, map_builder.player_start);
        map_builder.rooms
            .iter()
            .skip(1)
            .map(|r| r.center())
            .for_each(|pos| spawn_monster(&mut ecs, &mut rng, pos));
        resources.insert(map_builder.map);
        resources.insert(Camera::new(map_builder.player_start));
        resources.insert(TurnState::AwaitingInput);
        Self { 
            ecs,
            resources,
            input_systems : build_input_scheduler(),
            player_systems : build_player_scheduler(),
            monster_systems : build_monster_scheduler(),
        }
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        ctx.set_active_console(0);
        ctx.cls();
        ctx.set_active_console(1);
        ctx.cls();
        self.resources.insert(ctx.key);
        let current_state = self.resources.get::<TurnState>().unwrap().clone();
        match current_state { 
            TurnState::AwaitingInput => self.input_systems.execute(&mut self.ecs, &mut self.resources),
            TurnState::PlayerTurn => self.player_systems.execute(&mut self.ecs, &mut self.resources),
            TurnState::MonsterTurn => self.monster_systems.execute(&mut self.ecs, &mut self.resources),
        }
        render_draw_buffer(ctx).expect("Render error");
        // TODO - Render Draw Buffer
    }
}

// https://stackoverflow.com/a/43093371
// https://medium.com/@ericdreichert/run-code-once-in-rust-e2b75be41c39 - this one uses an older format, but was still useful
static LOG_INIT_OBJ: Once = Once::new();
fn init_logging() {
    LOG_INIT_OBJ.call_once(|| {
        let _ = log4rs::init_file("config/log4rs.yaml", Default::default()).expect("Failed to initialize logging.");
    });
}

fn init_graphics() -> BTerm {
    let context = BTermBuilder::new()
        .with_title("Rust Roguelike")
        .with_fps_cap(30.0)
        .with_dimensions(DISPLAY_WIDTH, DISPLAY_HEIGHT)
        .with_tile_dimensions(32, 32)
        .with_resource_path("resources/")
        .with_font("terminal8x8.png", 8, 8)
        .with_simple_console(DISPLAY_WIDTH, DISPLAY_HEIGHT, "terminal8x8.png")
        .with_simple_console_no_bg(DISPLAY_WIDTH, DISPLAY_HEIGHT, "terminal8x8.png")
        .build()
        .expect("Failed to initialize graphics.");

   context
}

fn main() -> BError {
    
    dotenv()?;
    
    init_logging();

    let bterm = init_graphics();

    main_loop(bterm, State::new())
}
