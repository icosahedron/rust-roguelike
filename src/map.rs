use crate::prelude::*;
use std::ops::Index;
use std::ops::IndexMut;

const NUM_TILES : usize = (SCREEN_HEIGHT * SCREEN_WIDTH) as usize;

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum TileType {
    Empty,
    Wall,
    Floor,
}

pub struct Map {
    pub tiles : Vec<TileType>,
    pub width : i32,
    pub height : i32,
}


impl Map {

    pub fn new() -> Self {
        Self {
            tiles : vec![TileType::Wall; NUM_TILES],
            width : SCREEN_WIDTH,
            height : SCREEN_HEIGHT,
        }
    }

    #[cfg(test)]
    pub fn of_size(x : i32, y : i32) -> Self { 
        Self {
            width : x,
            height : y,
            tiles : vec![TileType::Wall; (x * y) as usize],
        }
    }

    pub fn map_idx(&self, x : i32, y : i32) -> usize {
        ((y * self.width) + x) as usize
    }

    pub fn can_enter_tile(&self, point : Point) -> bool {
        self.in_bounds(point) 
            && self.tiles[self.map_idx(point.x, point.y)] == TileType::Floor
    }

    pub fn in_bounds(&self, point : Point) -> bool {
        point.x >= 0 && point.x < self.width
            && point.y >= 0 && point.y < self.height
    }

    pub fn try_idx(&self, point : Point) -> Option<usize> {
        if !self.in_bounds(point) {
            None
        }
        else {
            Some(self.map_idx(point.x, point.y))
        }
    }
}

impl Index<(i32, i32)> for Map {
    type Output = TileType;

    fn index(&self, i: (i32, i32)) -> &TileType {
        let idx : usize = ((i.1 * self.width) + i.0) as usize;
        &self.tiles[idx]
    }
}

impl IndexMut<(i32, i32)> for Map {
    fn index_mut(&mut self, i: (i32, i32)) -> &mut TileType {
        let idx : usize = ((i.1 * self.width) + i.0) as usize;
        &mut self.tiles[idx]
    }
}

impl Index<Point> for Map {
    type Output = TileType;

    fn index(&self, pt: Point) -> &
    TileType {
        let idx : usize = ((pt.y * self.width) + pt.x) as usize;
        &self.tiles[idx]
    }
}

impl IndexMut<Point> for Map {
    fn index_mut(&mut self, pt: Point) -> &mut TileType {
        let idx : usize = ((pt.y * self.width) + pt.x) as usize;
        &mut self.tiles[idx]
    }
}
