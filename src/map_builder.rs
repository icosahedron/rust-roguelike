use crate::prelude::*;
use std::collections::HashMap;

const NUM_ROOMS : usize = 20;

// precalculated tile offsets to check when removing wall tiles that don't bound a room.
lazy_static! {
    static ref SIDES_TO_COORD : HashMap<(bool, bool, bool, bool), Vec<(i32, i32)>> = HashMap::from([
        ((true, false, true, false), vec![(-1, -1), (-1, 0), (0, -1)]),
        ((true, false, false, true), vec![(1, 0), (-1, 1), (-1, 0)]),
        ((true, false, true, true), vec![(-1, -1), (-1, 0), (0, -1), (1, 0), (-1, 1)]),
        ((false, true, true, false), vec![(0, -1), (1, -1), (1, 0)]),
        ((false, true, false, true), vec![(1, 0), (1, 1), (0, 1)]),
        ((false, true, true, true), vec![(0, -1), (1, -1), (1, 0), (1, 1), (0, 1)]),
        ((true, true, true, false), vec![(-1, 0), (-1, -1), (0, -1), (1, -1), (1, 0)]),
        ((true, true, false, true), vec![(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0)]),
        ((true, false, true, true), vec![(0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1)]),
        ((true, true, true, true), vec![(-1, -1), (0, -1), (1, -1), (1, 0), (1, 1), (1, 0), (-1, 1), (-1, 0)]),
    ]); 
}

pub struct MapBuilder {
    pub map : Map,
    pub rooms : Vec<Rect>,
    pub player_start : Point,
}

impl MapBuilder {
    fn apply_horizontal_tunnel(&mut self, x1: i32, x2: i32, y: i32) {
        use std::cmp::{min, max};

        for x in min(x1, x2) ..= max(x1, x2) {
            if let Some(_) = self.map.try_idx(Point::new(x, y)) {
                self.map[(x, y)] = TileType::Floor;
            }
        }
    }

    fn apply_vertical_tunnel(&mut self, y1: i32, y2: i32, x: i32) {
        use std::cmp::{min, max};

        for y in min(y1, y2) ..= max(y1, y2) {
            if let Some(_) = self.map.try_idx(Point::new(x, y)) {
                self.map[(x, y)] = TileType::Floor;
            }
        }
    }

    fn build_corridors(&mut self, rng: &mut RandomNumberGenerator) { 
        let mut rooms = self.rooms.clone();
        rooms.sort_by(|a,b| a.center().x.cmp(&b.center().x));
        for (i, room) in rooms.iter().enumerate().skip(1) { 
            let prev = rooms[i-1].center();
            let new = room.center();
            if rng.range(0,2) == 1 { 
                self.apply_horizontal_tunnel(prev.x, new.x, prev.y); 
                self.apply_vertical_tunnel(prev.y, new.y, new.x);
            } 
            else {
                self.apply_vertical_tunnel(prev.y, new.y, prev.x); 
                self.apply_horizontal_tunnel(prev.x, new.x, new.y);
            }
        }
    }

    fn build_random_rooms(&mut self, rng : &mut RandomNumberGenerator) {
        while self.rooms.len() < NUM_ROOMS {
            let room = Rect::with_size(
                rng.range(1, self.map.width - 10),
                rng.range(1, self.map.height - 10),
                rng.range(2, 10),
                rng.range(2, 10),
            );
            let mut overlap = false;
            for r in self.rooms.iter() {
                if r.intersect(&room) {
                    overlap = true;
                }
            }
            if !overlap {
                room.for_each(|p| 
                    if p.x > 0 && p.x < self.map.width 
                        && p.y > 0 && p.y < self.map.height {
                            self.map[(p.x, p.y)]= TileType::Floor;
                        }
                );

                self.rooms.push(room);
            }
        }
    }

    fn fill(&mut self, tile : TileType) {
        self.map.tiles.iter_mut().for_each(|t| *t = tile);
    }

    #[debug_requires(x >= 0 && x < self.map.width && y >= 0 && y < self.map.height, "Tile coords out of bounds")]
    fn make_empty_from_solid(&mut self, x : i32, y : i32) {
        // only make empty if it's a wall or empty already
        if self.map[(x, y)] == TileType::Floor {
            debug!("make_empty_from_solid: tile ({}, {}) already a floor", x, y);
            return;
        }
        // see which sides we should check.
        let mut top = false;
        let mut right = false;
        let mut left = false;
        let mut bottom = false;
        if x - 1 >= 0 { left = true };
        if x + 1 < self.map.width { right = true };
        if y - 1 >= 0  { top = true };
        if y + 1 < self.map.height { bottom = true };
        // use precalculated offsets to check which tiles are walls or empty already
        debug!("make_empty_from_solid: x = {}, y = {}, left = {}, right = {}, top = {}, bottom = {}", x, y, left, right, top, bottom);
        if let Some(tiles) = SIDES_TO_COORD.get(&(left, right, top, bottom)) {
            let mut empty = true;
            for tile in tiles {
                debug!("make_empty_from_solid: looking at tile {}, {}, map = {:?}", x + tile.0, y + tile.1, self.map[(x + tile.0, y + tile.1)]);
                empty &= self.map[(x + tile.0, y + tile.1)] != TileType::Floor;
            }
            // if all the tiles are walls or empty already, make this one empty too
            if empty {
                self.map[(x, y)] = TileType::Empty;
            }
        }
    } 

    fn remove_solid_rock(&mut self, w : i32, h : i32) {
        for y in 0..h {
            for x in 0..w {
                self.make_empty_from_solid(x, y);
            }
        }
    }

    pub fn new(rng : &mut RandomNumberGenerator) -> Self {
        let mut mb = MapBuilder {
            map : Map::new(),
            rooms : Vec::new(),
            player_start : Point::zero(),
        };
        mb.fill(TileType::Wall);
        
        mb.build_random_rooms(rng);
        mb.build_corridors(rng);
        mb.remove_solid_rock(SCREEN_WIDTH, SCREEN_HEIGHT);
        mb.player_start = mb.rooms[0].center();
        mb
    }

}

#[cfg(test)]
mod test {
    use super::*;
    use crate::init_logging;    
    use test_case::test_case;

    pub fn setup() {
        init_logging();
    }

    fn do_vecs_match<T: PartialEq>(a: &Vec<T>, b: &Vec<T>) -> bool {
        let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
        matching == a.len() && matching == b.len()
    }

    #[test_case((0, 0), vec![TileType::Empty, TileType::Wall, TileType::Wall, TileType::Wall] ; "nw corner should remove wall")]
    #[test_case((1, 0), vec![TileType::Wall, TileType::Empty, TileType::Wall, TileType::Wall] ; "ne corner should remove wall")]
    #[test_case((1, 1), vec![TileType::Wall, TileType::Wall, TileType::Wall, TileType::Empty] ; "se corner should remove wall")]
    #[test_case((0, 1), vec![TileType::Wall, TileType::Wall, TileType::Empty, TileType::Wall] ; "sw corner should remove wall")]
    fn test_remove_solid_walls_corner_should_remove(corner : (i32, i32), expected : Vec<TileType>) {
        setup();
        let map = Map::of_size(2,2);
        let mut map_builder = MapBuilder { map : map, rooms : Vec::new(), player_start : Point::zero() };
        map_builder.make_empty_from_solid(corner.0, corner.1);
        assert!(do_vecs_match(&map_builder.map.tiles, &expected));
    }

    #[test_case((0, 0), (1, 1), vec![TileType::Wall, TileType::Wall, TileType::Wall, TileType::Floor] ; "nw corner should not remove wall")]
    #[test_case((1, 0), (0, 1), vec![TileType::Wall, TileType::Wall, TileType::Floor, TileType::Wall] ; "ne corner should not remove wall")]
    #[test_case((1, 1), (0, 0), vec![TileType::Floor, TileType::Wall, TileType::Wall, TileType::Wall] ; "se corner should not remove wall")]
    #[test_case((0, 1), (1, 0), vec![TileType::Wall, TileType::Floor, TileType::Wall, TileType::Wall] ; "sw corner should not remove wall")]
    fn test_remove_corner_should_not_remove(corner : (i32, i32), floor_tile: (i32, i32), expected : Vec<TileType>) {
        setup();
        let mut map = Map::of_size(2,2);
        map[floor_tile] = TileType::Floor;
        let mut map_builder = MapBuilder { map : map, rooms : Vec::new(), player_start : Point::zero() };
        map_builder.make_empty_from_solid(corner.0, corner.1);
        assert!(do_vecs_match(&map_builder.map.tiles, &expected));
    }

    #[test_case(vec![TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Empty, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall] ; "should remove center of all walls")]
    #[test_case(vec![TileType::Wall, TileType::Empty, TileType::Wall, TileType::Wall, TileType::Empty, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall] ; "should remove center of 1 empty")]
    #[test_case(vec![TileType::Wall, TileType::Empty, TileType::Empty, TileType::Empty, TileType::Empty, TileType::Empty, TileType::Empty, TileType::Empty, TileType::Empty] ; "should remove center of all empty")]
    fn test_remove_center_should_remove_center(expected : Vec<TileType>) {
        setup();
        let mut map = Map::of_size(3, 3);
        map.tiles = expected.clone();
        let mut map_builder = MapBuilder { map : map, rooms : Vec::new(), player_start : Point::zero() };
        map_builder.make_empty_from_solid(1, 1);
        assert!(do_vecs_match(&map_builder.map.tiles, &expected));
    }

    #[test_case((0, 0), vec![TileType::Floor, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Empty, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall] ; "should not remove center due to floor in corner")]
    #[test_case((1, 1), vec![TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Floor, TileType::Wall, TileType::Wall, TileType::Wall, TileType::Wall] ; "should not remove center due to floor in center")]
    fn test_remove_center_should_not_remove_center(floor : (i32, i32), expected : Vec<TileType>) {
        setup();
        let mut map = Map::of_size(3, 3);
        map.tiles = expected.clone();
        map[floor] = TileType::Floor;
        let mut map_builder = MapBuilder { map : map, rooms : Vec::new(), player_start : Point::zero() };
        map_builder.make_empty_from_solid(1, 1);
        assert!(do_vecs_match(&map_builder.map.tiles, &expected));
    }

}

