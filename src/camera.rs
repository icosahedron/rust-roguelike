use crate::prelude::*;

pub struct Camera {
    pub left_x : i32,
    pub right_x : i32,
    pub top_y : i32,
    pub bottom_y : i32,
}

impl Camera {
    pub fn new(player_position : Point) -> Self {
        let mut camera = Self {
            left_x : 0,
            right_x : 0,
            top_y : 0,
            bottom_y : 0,
        };
        camera.on_player_move(player_position);
        camera
    }

    pub fn on_player_move(&mut self, player_position : Point) {
        self.left_x = player_position.x - DISPLAY_WIDTH / 2;
        self.right_x = player_position.x + DISPLAY_WIDTH / 2;
        if self.left_x < 0 {
            self.left_x = 0;
            self.right_x = DISPLAY_WIDTH;
        }
        else if self.right_x > SCREEN_WIDTH {
            self.left_x = SCREEN_WIDTH - DISPLAY_WIDTH;
            self.right_x = SCREEN_WIDTH;
        }
        self.top_y = player_position.y - DISPLAY_HEIGHT / 2;
        // + 1 to account for rounding since DISPLAY_HEIGHT / 2 rounds down 
        // and leaves a blank line on the bottom of the map
        self.bottom_y = player_position.y + DISPLAY_HEIGHT / 2 + 1;
        if self.top_y < 0 {
            self.top_y = 0;
            self.bottom_y = DISPLAY_HEIGHT;
        }
        else if self.bottom_y > SCREEN_HEIGHT {
            self.top_y = SCREEN_HEIGHT - DISPLAY_HEIGHT;
            self.bottom_y = SCREEN_HEIGHT;
        }
    }    
}