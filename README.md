# Hands on Rust Roguelike

Learning Rust one dungeon level at a time.

The book "Hands on Rust" teaches Rust through building a roguelike.

The Bracket-lib library is used for most of the internals, so there 
is support up front, rather than writing from scratch.  This is 
both good and bad, as many learning opportunities are solved for you.
That said, the book would have been huge without using something like
Bracket-lib, a fair trade-off.

## Project Status

Chapters 1-2 are just introduction and set up, and Chapter 3 creates a "flappy
dragon" Flappy Bird clone.  It serves as a good introduction. Chapter 4 is 
a brief design document for the roguelike to follow.

- [X] Chapter 5
- [X] Chapter 6
- [X] Chapter 7
- [ ] Chapter 8
- [ ] Chapter 9
- [ ] Chapter 10
- [ ] Chapter 11
- [ ] Chapter 12
- [ ] Chapter 13
- [ ] Chapter 14
- [ ] Chapter 15
- [ ] Chapter 16

The default bracket-lib configuration is used, a window running OpenGL.

```
bracket-lib = "~0.8.1"
# bracket-lib = { version = "~0.8.1", default_features = false, features = ["curses"] }
```

I would have liked to use `crossterm` instead, but it didn't compile properly.

## Implementation Notes

I love Rust. So many cool things. I do wish there was an automatic conversion, instead
of having to use `into` for certain things.  I would also love custom operators.

Up until chapter 7, I felt I was learning a lot about Rust, but I don't feel like Chapter
7 really moved the needle on my Rust knowledge, though I did learn about legion more.
And that has given me ideas on how I might want to do my own ECS, though I'll probably
just look at something like `Bevy` more.

I am also keen on getting `crossterm` to work.  I'll have to dig into that more later
perhaps.  `bracket-lib` provides a lot of convenience, and I'd hate to lose that.

## Future Plans

Lots of those, but I'll forbear on writing that until I at least finish the book.
